#!/bin/bash

if [[ ! -d /home/$USER/demo ]]; then
	mkdir -p /home/$USER/demo
fi

WORKDIR=/home/$USER/demo
cd $WORKDIR

echo -e '\tPulling code from remote..\n'
git pull origin master
echo -e '\tCompiling package\n'
mvn clean package
echo -e '\tContainerize Application\n'
docker build -t demo:latest .
echo -e '\tDeploy Container\n'
docker run -d --net=host --name demo --rm demo:latest 
