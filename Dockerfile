FROM openjdk:8-jdk as demo

ENV APP_NAME DEFAULT

COPY target /root/app 
COPY entrypoint.sh /tmp/entrypoint.sh
WORKDIR /root/app

EXPOSE 8080
CMD ["/tmp/entrypoint.sh"] 
